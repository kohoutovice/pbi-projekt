#!/usr/bin/env python
from pymol import cmd
import random
import numpy as np
from time import sleep
import threading
from scipy import spatial
import argparse


class Molecular:
    def __init__(self):
        self.xrot = 0.0
        self.yrot = 0.0
        self.zrot = 0.0

    def get_coords(self):
        return cmd.get_coords(self.name, 1).tolist()

    def get_coord(self):
        return self.get_coords()[0]

    def set_coord(self, target):
        coord = self.get_coord()
        trans = (np.array(target) - np.array(coord)).tolist()
        cmd.translate(trans, self.name, state=0, camera=0)

    def rotate(self, vector):
        coord = self.get_coord()
        cmd.rotate("x", angle=vector[0], selection=self.name, camera = 0, origin=coord)
        cmd.rotate("y", angle=vector[1], selection=self.name, camera = 0, origin=coord)
        cmd.rotate("z", angle=vector[2], selection=self.name, camera = 0, origin=coord)

        self.xrot += vector[0]
        self.yrot += vector[1]
        self.zrot += vector[2]

        self.xrot = self.xrot % 360
        self.yrot = self.yrot % 360
        self.zrot = self.zrot % 360

    def set_rotation(self, vector):
        coord = self.get_coord()
        vector = np.array([self.xrot, self.yrot, self.zrot, ]) - np.array(vector)
        cmd.rotate("x", angle=vector[0], selection=self.name, camera = 0, origin=coord)
        cmd.rotate("y", angle=vector[1], selection=self.name, camera = 0, origin=coord)
        cmd.rotate("z", angle=vector[2], selection=self.name, camera = 0, origin=coord)

        self.xrot += vector[0]
        self.yrot += vector[1]
        self.zrot += vector[2]

    def get_position(self):
        coord = self.get_coord()
        return np.array([coord[0], coord[1], coord[2], self.xrot, self.yrot, self.zrot])

    def set_position(self, position):
        self.set_coord(position[:3])

    def move(self, vector):
        vector = vector.tolist()
        cmd.translate(vector[:3], self.name, state=0, camera=0)
        self.rotate(vector[3:])

    def hide(self):
        cmd.hide(representation="everything", selection=self.name)

    def disable(self):
        cmd.disable(self.name)

    def enable(self):
        cmd.enable(self.name)

    def show(self, what):
        cmd.show(what, self.name)

    def color(self, c):
        cmd.color(c, self.name)


class Particle(Molecular):
    def __init__(self, structure, name):
        super(Particle, self).__init__()
        self.name = name
        self.structure = structure
        cmd.fab(structure, name)
        self.speed = np.random.uniform(low=0, high=1, size=(6,))
        self.best_position = None
        self.best_fitness = None
        self.atom_count = cmd.count_atoms(self.name)

    def distance(self, kd_tree):
        distance, index = kd_tree.query(self.get_coords())
        return distance

    def contacts_fitness(self, kd_tree, prot_size):
        min_distance = 3.2
        max_distance = 5
        distance = self.distance(kd_tree)
        distance = distance-min_distance

        contacts_count = len(list(filter(lambda x: x > 0 and x < max_distance - min_distance, distance.tolist())))

        fit_contacts = (1/(contacts_count)*(prot_size/self.atom_count)) if contacts_count > 0 else (1/(0.1)*(prot_size/self.atom_count))
        fit = fit_contacts
        if self.best_fitness is None:
            self.best_fitness = fit
            self.best_position = self.get_position()

        if fit < self.best_fitness:
            self.best_fitness = fit
            self.best_position = self.get_position()
        return fit

    def combination_fitness(self, kd_tree, prot_size):
        min_distance = 3.2
        max_distance = 5
        distance = self.distance(kd_tree)
        distance = distance-min_distance
        contacts_count = len(list(filter(lambda x: x > 0 and x < max_distance - min_distance, distance.tolist())))

        fit_contacts = (1/contacts_count)*(prot_size) if contacts_count > 0 else (1/self.atom_count)*(prot_size)
        fit_distance = (distance*distance).sum()
        fit = fit_distance + fit_contacts
        if self.best_fitness is None:
            self.best_fitness = fit
            self.best_position = self.get_position()

        if fit < self.best_fitness:
            self.best_fitness = fit
            self.best_position = self.get_position()
        return fit

    def distance_fitness(self, kd_tree, prot_size):
        min_distance = 3.2
        distance = self.distance(kd_tree)
        distance = distance-min_distance
        fit = (distance*distance).sum()
        if self.best_fitness is None:
            self.best_fitness = fit
            self.best_position = self.get_position()

        if fit < self.best_fitness:
            self.best_fitness = fit
            self.best_position = self.get_position()
        return fit

    def compute_speed(self, swarm_best):
        position = self.get_position()
        w = 0.5
        cp = 0.3
        cg = 0.3
        rp = random.uniform(0, 1)
        rg = random.uniform(0, 1)

        new_speed = w * self.speed + \
            cp * rp * (self.best_position - position) + \
            cg * rg * (swarm_best - position)

        return new_speed

    def step(self, swarm_best):
        self.move(self.speed)
        self.speed = self.compute_speed(swarm_best)


class Protein(Molecular):
    """docstring for Protein."""
    def __init__(self, name):
        super(Protein, self).__init__()
        self.name = name
        cmd.fetch(name)
        self.atom_count = cmd.count_atoms(self.name)

    def coord_frame(self):
        coords = cmd.get_coords(self.name, 1).tolist()
        x_min = coords[0][0]
        x_max = coords[0][0]
        y_min = coords[0][1]
        y_max = coords[0][1]
        z_min = coords[0][2]
        z_max = coords[0][2]
        for coord in coords:
            x, y, z = coord[0], coord[1], coord[2]
            if x < x_min:
                x_min = x
            if x > x_max:
                x_max = x

            if y < y_min:
                y_min = y
            if y > y_max:
                y_max = y

            if z < z_min:
                z_min = z
            if z > z_max:
                z_max = z
        return (x_min, x_max), (y_min, y_max), (z_min, z_max)

    def get_kd_tree(self):
        return spatial.KDTree(self.get_coords())


class Swarm(object):
    def __init__(self, protein_name, molecule_structure, swarm_count):
        self.particles = []
        for i in range(swarm_count):
            self.particles.append(Particle(molecule_structure, str(molecule_structure) + str(i)))
        self.protein = Protein(protein_name)
        self.xf, self.yf, self.zf = self.protein.coord_frame()
        self.init_positions()

    def init_positions(self):
        for particle in self.particles:
            x = random.uniform(self.xf[0], self.xf[1])
            y = random.uniform(self.yf[0], self.yf[1])
            z = random.uniform(self.zf[0], self.zf[1])
            particle.set_coord([x, y, z])
            xrot = random.uniform(0, 360)
            yrot = random.uniform(0, 360)
            zrot = random.uniform(0, 360)
            particle.rotate([xrot, yrot, zrot])
        cmd.refresh()

    def run(self, epochs, fitness_f_name):
        # just init best positions of swarm
        kd = self.protein.get_kd_tree()
        best_fitness = None
        best_position = None

        for particle in self.particles:
            fitness_f = {
                'distance': particle.distance_fitness,
                'combination': particle.combination_fitness,
                'contacts': particle.contacts_fitness
            }[fitness_f_name]
            fitness = fitness_f(kd, self.protein.atom_count)
            if best_fitness is None:
                best_fitness = fitness
                best_position = particle.get_position()
            if fitness < best_fitness:
                best_fitness = fitness
                best_position = particle.get_position()

        for i in range(epochs):
            for particle in self.particles:
                fitness_f = {
                    'distance': particle.distance_fitness,
                    'combination': particle.combination_fitness,
                    'contacts': particle.contacts_fitness
                }[fitness_f_name]
                fitness = fitness_f(kd, self.protein.atom_count)
                if fitness < best_fitness:
                    best_fitness = fitness
                    best_position = particle.get_position()
                    print(particle.distance(kd))

            for particle in self.particles:
                particle.step(best_position)
            print("Epoch:", i, "Fitness:", best_fitness)

        #Just hide everything(except best particle)
        for particle in self.particles:
            particle.disable()

        self.particles[0].enable()
        self.particles[0].set_position(best_position)
        self.particles[0].show("surface")

        self.protein.show("surface")
        self.protein.color("blue")

        print("=====================================================")
        print("Results:")
        print("Best fitness:", best_fitness)
        print("Atom distances:", particle.distance(kd))
        print("=====================================================")
        return best_fitness, best_position


def main():
    parser = argparse.ArgumentParser(description='Protein docking using swarm optimization algorithm.')
    parser.add_argument('--fitness', type=str, choices=['distance', 'combination', 'contacts'], default="distance", help="Choose optimization method")
    parser.add_argument('--protein', type=str, default="1BAG", help="Set protein via PDB code.")
    parser.add_argument('--molecule', type=str, default="ALALA", help="Set ligand via string sequence.")
    parser.add_argument('--seed', type=int, default=None, help="Set random seed")
    parser.add_argument('--epochs', type=int, default=200, help="Set epochs count")
    parser.add_argument('--particles', type=int, default=40, help="Set particle count")
    args = parser.parse_args()

    if args.seed:
        random.seed(args.seed)

    cmd.reinitialize()
    s = Swarm(args.protein, args.molecule, args.particles)
    fit, pos = s.run(args.epochs, args.fitness)


x = threading.Thread(target=main)
x.start()
